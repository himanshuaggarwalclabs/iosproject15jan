//
//  ViewController.swift
//  myClass
//
//  Created by Samar Singla on 14/01/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    // Text Fields
    @IBOutlet weak var sName: UITextField!
    
    @IBOutlet weak var sAge: UITextField!
    
    @IBOutlet weak var sClass: UITextField!
    
    @IBOutlet weak var sM1: UITextField!
    
    @IBOutlet weak var sM2: UITextField!
    
    @IBOutlet weak var sM3: UITextField!
    
    @IBOutlet weak var sM4: UITextField!
    
    @IBOutlet weak var sM5: UITextField!
    
    // Labels
    
    @IBOutlet weak var disName: UILabel!
   
    @IBOutlet weak var disAge: UILabel!
    
    @IBOutlet weak var disClass: UILabel!
    
    @IBOutlet weak var disPersentage: UILabel!
    
    @IBOutlet weak var validationLabel: UILabel!
    
    @IBOutlet weak var resultImage: UIImageView!
    
    @IBOutlet weak var disTmarks: UILabel!
    
    @IBAction func submitButton(sender: AnyObject) {
        
    // Validation Code
        if sName.text.isEmpty {
            sName.text = " Enter Name"
         } else if sAge.text.isEmpty {
            sAge.text = " Enter Age"
            
         } else if sClass.text.isEmpty{
            sClass.text = " Enter Class"
            
         } else if sM1.text.isEmpty {
            sM1.text = " Enter Marks1"
            
        } else if sM2.text.isEmpty{
            sM2.text = " Enter Marks2"
       
        } else if sM3.text.isEmpty {
            sM3.text = " Enter Marks3"
       
       } else if sM4.text.isEmpty {
            sM4.text = " Enter Marks4"
       
       } else if sM5.text.isEmpty {
            sM5.text = " Enter Marks5"
            
        } else
      
    // Main Execution Code
            
        {
               // Convert Marks TextFiels into Integers
             var m1 = sM1.text.toInt()
             var m2 = sM2.text.toInt()
             var m3 = sM3.text.toInt()
             var m4 = sM4.text.toInt()
             var m5 = sM5.text.toInt()
            
               // TotalMarks
             var totalMarks = (m1!+m2!+m3!+m4!+m5!)
            
               // Overall Persentage
             var Persentage = totalMarks/5
            
               // Displaying TextFields in  Labels
             disName.text = "Student Name is \(sName.text)"
             disAge.text = "Student Age is \(sAge.text)"
             disClass.text = "Student Class is \(sClass.text)"
             disTmarks.text = " Student Totalmarks is \(totalMarks)"
             disPersentage.text = " Student persentage is \(Persentage)%"
            
               // Assign  Different Images to Students Performance 
               // 40%-60% = ***
             if Persentage>40 && Persentage<=60 {
             var image = UIImage(named:"3star.jpeg")
            
                 resultImage.image = image
                // 60%-80% = ****
              } else if Persentage>60 && Persentage<=80 {
                var image = UIImage(named:"4star.jpeg")
             
                resultImage.image = image
                // 80%-100% = ****
              } else if Persentage>80 && Persentage<=100 {
                 var image = UIImage(named:"5star.jpeg")
            
                     resultImage.image = image
                // 20%-40% = **
              } else if Persentage>20 && Persentage<40 {
                 var image = UIImage(named:"2star.jpeg")
            
                   resultImage.image = image
                // 00%-20% = *
              } else if Persentage>=0 && Persentage<=20 {
                 var image = UIImage(named:"1star.jpeg")
            
                   resultImage.image = image
              }
        
    
       }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
