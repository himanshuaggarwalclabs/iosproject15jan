//
//  ViewController.swift
//  myCalculator
//
//  Created by Samar Singla on 13/01/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // Display TextFields
    @IBOutlet weak var calDisplay: UITextField!
    // Globel Variabls
    var isTypingNumber = false
    var dotPoint = false
    var firstNumber = 0.0
    var secondNumber = 0.0
    var operation = ""
    
    // Numbers Button
    @IBAction func numberPressed(sender: AnyObject) {
        var number = sender.currentTitle
        
        if isTypingNumber {
            calDisplay.text = calDisplay.text! + number!!
            
        } else {
            calDisplay.text = number
            isTypingNumber = true
        }
        
                                               //NUMBERS
    }
    
    // CalculationButton
    @IBAction func calTapped(sender: AnyObject) {
        isTypingNumber = false
         dotPoint = false
        firstNumber = (calDisplay.text as NSString).doubleValue
        operation = sender.currentTitle!!
        
                                               //CALCULATION
    }
     // EqualButton
    @IBAction func equalTapped(sender: AnyObject) {
        isTypingNumber = false
        dotPoint = false
        var result = 0.0
        secondNumber = (calDisplay.text as NSString).doubleValue
        
        if operation == "+" {
            result = firstNumber + secondNumber
        } else if operation == "-" {
             result = self.firstNumber - self.secondNumber
        } else if operation == "/" {
             result = self.firstNumber / self.secondNumber
        } else if operation == "*" {
            result = self.firstNumber * self.secondNumber
        }
        
        calDisplay.text = "\(result)"
        
                                              //EQUAL
    }
    
     //ClearButton
    @IBAction func clearButton(sender: AnyObject) {
        
        dotPoint = false
        calDisplay.text = ""
       
                                             //CLEAR
    }
    
     // DotButton
    @IBAction func dotButton(sender: AnyObject) {
        
        if dotPoint == false {
            var num = "."
            if isTypingNumber {
                calDisplay.text = calDisplay.text! + num
                
            } else {
                calDisplay.text = num
                isTypingNumber = true
            }
        }
        dotPoint = true
            
                                            //DOT
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

